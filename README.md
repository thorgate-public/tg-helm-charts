# Thorgate Helm Chart repository

## How to add this repository

```shell script
$ helm repo add tg-helm-charts https://thorgate-public.gitlab.io/tg-helm-charts/
> "tg-helm-charts" has been added to your repositories
```
