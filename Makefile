# Project specific settings
PROJECT_ROOT ?= $(CURDIR)
CHARTS_DIR ?= $(PROJECT_ROOT)/charts
VALUES_DIR ?= $(PROJECT_ROOT)/values
GENERATED_DIR ?= $(PROJECT_ROOT)/generated

# Mark non-file targets as PHONY
.PHONY: dependencies template guard-%

guard-%:
	@ if [ "${${*}}" = "" ]; then \
		echo "Environment variable $* not set"; \
		exit 1; \
	fi

ensure-chart-exists: guard-CHART
	@ if [ ! -f "$(CHARTS_DIR)/$(CHART)/Chart.yaml" ]; then \
		echo "Chart $(CHART) does not exist!"; \
		exit 1; \
	fi


$(GENERATED_DIR):
	@mkdir -p $(GENERATED_DIR)


$(VALUES_DIR):
	@mkdir -p $(VALUES_DIR)


dependencies:
	@helm dependency build charts/*


render-template: ensure-chart-exists $(GENERATED_DIR) $(VALUES_DIR)
	@helm template -n testing $(CHARTS_DIR)/$(CHART) -f $(VALUES_DIR)/$(CHART).yaml > $(GENERATED_DIR)/$(CHART).yaml


watch-and-render: ensure-chart-exists
	@echo "Watching for $(CHART) changes"
	@ while true; do \
		printf "=%.0s" {1..81} ; printf "\n"; \
		inotifywait -q --exclude ".*(\.git|\.idea)" -r "$(VALUES_DIR)/$(CHART).yaml" "$(CHARTS_DIR)/$(CHART)" -e create,modify,delete; \
		make render-template && echo "Rendered template successfully"; \
	done
