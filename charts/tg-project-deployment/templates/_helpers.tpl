{{/* vim: set filetype=mustache: */}}

{{/*
Generic helpers
*/}}
{{- define "clean-name" -}}
{{- . | trimSuffix "-app" | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{/*
Validate applicationName
*/}}
{{- define "tg-project-deployment.name" -}}
{{- include "clean-name" (required "applicationName is required for deployment" .Values.applicationName) -}}
{{- end -}}

{{/*
To generate the fullnames we need access to both the Chart/Release/Value
but we use them inside `range` and we need access to the deployment name.

So when using this tg-project-deployment.fullname template, pass in context (aka pipeline) like:
`dict "parent" $ "deployment" .`
*/}}
{{- define "tg-project-deployment.fullname" -}}
{{- include "clean-name" (printf "%s-%s-%s" .parent.Release.Name .name (include "tg-project-deployment.name" .parent)) -}}
{{- end -}}

{{- define "tg-project-deployment.ingress-fullname" -}}
{{- include "clean-name" (printf "%s-%s" .Release.Name "ingress") -}}
{{- end -}}

{{/*
Subchart helper overrides
*/}}
{{- define "redis.fullname" -}}
{{- printf "%s-%s" .Release.Name "redis" | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{/*
Get a escaped regex from URL
*/}}
{{- define "host.escape" -}}
{{- . | replace "." "\\." -}}
{{- end -}}

{{/*
Get a hostname from URL
*/}}
{{- define "hostname" -}}
{{- . | trimPrefix "http://" |  trimPrefix "https://" |  trimPrefix "www." | trimSuffix "/" -}}
{{- end -}}
