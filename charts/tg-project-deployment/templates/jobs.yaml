{{- $appName := include "tg-project-deployment.name" . -}}
{{- range $jobName, $job := .Values.jobs -}}
{{- $fullName := include "tg-project-deployment.fullname" (dict "name" $jobName "parent" $) -}}
{{- $sourceApplication := index $.Values.applications (required "Source application \"valuesFromApplication\" is required for JOB" $job.valuesFromApplication) -}}
{{- $application := merge (dict) $job $sourceApplication -}}
---
apiVersion: batch/v1
kind: Job
metadata:
  name: "{{ $fullName }}"
  labels:
    jobName: "{{ $jobName }}"
    app: "{{ $fullName }}"
    {{- if $application.tier }}
    tier: "{{ $application.tier }}"
    {{- end }}
    app.kubernetes.io/managed-by: "{{ $.Release.Service }}"
    app.kubernetes.io/instance: "{{ $fullName }}"
    helm.sh/chart: "{{ $.Chart.Name }}-{{ $.Chart.Version | replace "+" "_" }}"
  {{ if $job.annotations }}
  annotations:
{{ toYaml $job.annotations | indent 4 }}
  {{ end }}
spec:
  template:
    metadata:
      name: "{{ $fullName }}"
    spec:
      {{ if $.Values.imagePullSecrets }}
      imagePullSecrets:
      {{- range $imagePullSecrets := $.Values.imagePullSecrets }}
      - name: {{ $imagePullSecrets }}
      {{- end }}
      {{- end }}
      containers:
      - name: "{{ $jobName }}"
        image: "{{ $application.image }}"
        {{- if $job.command }}
        command: ["/bin/sh"]
        args: ["-c", "{{ $job.command }}"]
        {{- end }}
        imagePullPolicy: {{ $.Values.imagePullPolicy }}
        resources:
{{ toYaml (default $.Values.resources $application.resources $job.resources) | indent 10 }}
        env:
        {{- range $name, $value := $application.environment }}
        - name: "{{ $name }}"
          value: "{{ $value }}"
        {{- end }}
        {{- range $name, $value := $.Values.environment }}
        - name: "{{ $name }}"
          value: "{{ $value }}"
        {{- end }}
        # Pass along _HOST for all other services in this deployment
        {{- range $serviceName, $service := $.Values.applications }}
        {{- if $service.port }}
        - name: {{ upper (printf "%s_HOST" $serviceName) }}
          value: {{ template "tg-project-deployment.fullname" (dict "name" $serviceName "parent" $) }}
        {{- end }}
        {{- end }}

        # Pass along _HOST for enabled subcharts
        {{- range $subchart := $.Values.optionalSubcharts }}
        {{- if (index $.Values $subchart).enabled }}
          - name: {{ upper (printf "%s_HOST" $subchart) }}
            value: {{ include (printf "%s.fullname" $subchart) $ }}
        {{- end -}}
        {{- end }}

        {{- if $application.externalSecrets }}
        volumeMounts:
        {{- range $secretName, $secretParams := $application.externalSecrets }}
        - name: {{ $secretName }}
          mountPath: "/var/secrets/{{ $secretName }}"
        {{- end }}
        {{- end }}

      {{- if $application.externalSecrets }}
      volumes:
      {{- range $secretName, $secretParams := $application.externalSecrets }}
      - name: {{ $secretName }}
        secret:
          secretName: {{ $secretName }}
      {{- end }}
      {{- end }}

      restartPolicy: {{ $job.restartPolicy | default "Never" }}

  backoffLimit: {{ $job.backoffLimit | default 6 }}
{{- end }}
