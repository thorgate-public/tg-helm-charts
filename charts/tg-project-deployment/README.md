# Thorgate django project template Helm chart

## TL;DR

```bash
# Install configuration
$ helm install tg-helm-charts/tg-project-deployment -f values.yaml
```

## Introduction
This is Thorgate project template helm chart for dynamic deployments.


## Prerequisites

- Kubernetes 1.12+
- PV provisioner support in the underlying infrastructure


## Installing the Chart

To install the chart with the release name `my-release`:

```bash
$ helm install --name my-release tg-helm-charts/tg-project-deployment
```

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```bash
$ helm delete my-release
```


## Configuration

The following table lists the configurable parameters of the Redis chart and their default values.

| Parameter                                                       | Description                                                                                                                                                                        | Default                                                 |
|-----------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------|
| `applicationName`                                               | Stack name. **Required**                                                                                                                                                           | `nil`                                                   |
| `horizontalAutoscaler.minReplicas`                              | Default minimum replicas for horizontal pod auto scaler.                                                                                                                           | `1`                                                     |
| `horizontalAutoscaler.maxReplicas`                              | Default maximum replicas for horizontal pod auto scaler.                                                                                                                           | `5`                                                     |
| `horizontalAutoscaler.targetCPUUtilizationPercentage`           | Default CPU utilization percentage.                                                                                                                                                | `80`                                                    |
| `imagePullPolicy`                                               | Container image policy. `Always` is recommended if `:latest` image tag is used.                                                                                                    | `IfNotPresent`                                          |
| `imagePullSecrets`                                              | List of secret names for container registries.                                                                                                                                     | `['secret']`                                            |
| `ingress.enabled`                                               | Enable Ingress service.                                                                                                                                                            | `false`                                                 |
| `ingress.ingressClass`                                          | Ingress controller class.                                                                                                                                                          | `nginx`                                                 |
| `ingress.tls.enabled`                                           | Enable Ingress SSL                                                                                                                                                                 | `true`                                                  |
| `ingress.tls.certIssuer`                                        | Cluster Certificate issuer. Required if `ingress.tls.enabled=true`                                                                                                                 | `nil`                                                   |
| `ingress.tls.annotations`                                       | Additional Ingress annotations. Default enabled: `kubernetes.io/ingress.class` and `certmanager.k8s.io/cluster-issuer` if `ingress.tls.enabled=true`.                              | `{}`                                                    |
| `environment`                                                   | Common environment to pass to all deployments.                                                                                                                                     | `{}`                                                    |
| `resources`                                                     | Default resource requests and limits for the application. See: https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/                               | `{}`                                                    |
| `applications.<name>.image`                                     | Application container image.                                                                                                                                                       | `nil`                                                   |
| `applications.<name>.tier`                                      | Application tier, used for labels only.                                                                                                                                            | `nil`                                                   |
| `applications.<name>.urlHost`                                   | Application URL, service and ingress configuration will be created if this is added.                                                                                               | `nil`                                                   |
| `applications.<name>.wwwTls`                                    | If ingress should request www prefixed certificate as well.                                                                                                                        | `false`                                                 |
| `applications.<name>.urlPath`                                   | Application URL path override                                                                                                                                                      | `/`                                                     |
| `applications.<name>.environmentSecret`                         | Pass in the name of a Secret which the deployment will load all key-value pairs from the Secret as environment variables in the application container.                             | `nil`                                                   |
| `applications.<name>.environmentSecretChecksum`                 | Pass in the checksum of the secrets referenced by `applications.<name>.environmentSecret`.                                                                                         | `nil`                                                   |
| `applications.<name>.command`                                   | Container command override. This should in string format.                                                                                                                          | `nil`                                                   |
| `applications.<name>.migrationCommand`                          | Container database migration command. This is optional and should be only passed in when `<name>` requires migration support. Migrations are handled before deployment is applied. | `nil`                                                   |
| `applications.<name>.replicas`                                  | Number of default replicas required for application deployment.                                                                                                                    | `1`                                                     |
| `applications.<name>.resources`                                 | Resource requests and limits for the specific application. See: https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/                              | `$.resources`                                           |
| `applications.<name>.hpa.enabled`                               | If true, enables horizontal pod autoscaler. A resource request is also required to be set, such as `resources.requests.cpu: 200m`.                                                 | `false`                                                 |
| `applications.<name>.hpa.minReplicas`                           | Override default minimum replicas to create for this applications.                                                                                                                 | `$.horizontalAutoscaler.minReplicas`                    |
| `applications.<name>.hpa.maxReplicas`                           | Override default maximum replicas to create for this applications.                                                                                                                 | `$.horizontalAutoscaler.maxReplicas`                    |
| `applications.<name>.hpa.targetCPUUtilizationPercentage`        | Override default CPU utilization percentage.                                                                                                                                       | `$.horizontalAutoscaler.targetCPUUtilizationPercentage` |
| `applications.<name>.port`                                      | Application service port. If not specified then service for this application is not created.                                                                                       | `nil`                                                   |
| `applications.<name>.readinessProbe`                            | See: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/                                                                                 | `nil`                                                   |
| `applications.<name>.livenessProbe`                             | See: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/                                                                                 | `nil`                                                   |
